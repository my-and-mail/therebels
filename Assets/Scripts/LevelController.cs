using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelController : MonoBehaviour
{
    public GameObject prefabBullet = null;
    public BulletData bulletData = null;
    public ItemView[] prefabItems;
    public MobView prefabMob = null;
    public UIEnemyIndicators uiEnemyIndicators;
    public Camera uiCamera = null;
    public RectTransform canvasRect = null;
    public CanvasScaler canvasScaler = null;

    public RectTransform moveImageBackground = null;
    public RectTransform mobileInputRect;

    RaycastHit[] bulletRaycastHits = new RaycastHit[5];
    List<IBullet> activeBullets = new List<IBullet>();
    List<IMob> activeMobs = new List<IMob>();
    public List<IMob> ActiveMobs => activeMobs;

    public static LevelController instance = null;

    Player mainPlayer = null;
    public Player MainPlayer => mainPlayer;

    private int levelMobCount = 5;


    ObjectPoolController objectPoolController = null;

    void Awake()
    {
        if (instance != null) {
            Debug.LogError("LevelController instance is duplicated!");
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        Application.targetFrameRate = 60;

        canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);
        objectPoolController = new ObjectPoolController();
        mainPlayer = FindObjectOfType<Player>();
    }

    void Start()
    {
        levelMobCount = Random.Range(5, 10);
        
        InitMobs();
        InitBullets();
        uiEnemyIndicators.Init(levelMobCount);

        SpawnMobs();
    }

    private void Update()
    {
        BulletsThink();
        MobsThink();
    }

    void BulletsThink()
    {
        for(int i = 0; i < activeBullets.Count; i++) {
            activeBullets[i].Think();

            if (!activeBullets[i].View.activeSelf) {
                activeBullets.RemoveAt(i);
                i--;
            }
        }        
    }

    void MobsThink()
    {
        for (int i = 0; i < activeMobs.Count; i++) {
            activeMobs[i].Think();
        }
    }
    
    void InitMobs()
    {
        for(int i = 0; i < levelMobCount; i++) {
            MobView mobView = GameObject.Instantiate(prefabMob, Vector3.zero, Quaternion.identity);
            mobView.gameObject.SetActive(false);

            IMob mob = new Mob(mobView);
            objectPoolController.PushObjectInInactiveObjectPool(ObjectPoolController.TYPE_MOB, mob);
        }       
    }

    void InitBullets()
    {
        for(int i = 0; i < levelMobCount * 10; i++) {
            GameObject bulletView =  GameObject.Instantiate(prefabBullet, Vector3.zero, Quaternion.identity);
            bulletView.SetActive(false);

            IBullet bullet = new Bullet(bulletData, bulletView);
            objectPoolController.PushObjectInInactiveObjectPool(ObjectPoolController.TYPE_BULLET, bullet);
        }
    }
    
    void SpawnMobs()
    {
        Vector3 playerPos = mainPlayer.transform.position;
        int mobCountInPool = objectPoolController.GetInactiveObjectCount(ObjectPoolController.TYPE_MOB);

        if (mobCountInPool < levelMobCount) {
            return;
        }

        for(int i = 0; i < levelMobCount; i++) {
            Vector3 randomDir = new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f));
            if (randomDir == Vector3.zero)  {
                randomDir = Vector3.right;
            }
            randomDir *= Random.Range(10.0f, 30.0f);
            SpawnMob(playerPos + randomDir, randomDir.normalized);
        }
    }

    void SpawnMob(Vector3 position, Vector3 dir)
    {
        IPoolable poolableMob = objectPoolController.GetObjectFromPool(ObjectPoolController.TYPE_MOB);
        poolableMob.Spawn(position, dir);
        poolableMob.OnViewRemoved += OnMobViewRemoved;
        objectPoolController.AddActiveObject(ObjectPoolController.TYPE_MOB, poolableMob);
        activeMobs.Add((IMob) poolableMob);
    }

    void OnMobViewRemoved(IPoolable mob)
    {
        // Debug.Log("OnMobRemove: " + mob.View.name);
        activeMobs.Remove((IMob)mob);
        mob.OnViewRemoved -= OnMobViewRemoved;
        objectPoolController.RemoveActiveObject(ObjectPoolController.TYPE_MOB, mob.View);
        objectPoolController.PushObjectInInactiveObjectPool(ObjectPoolController.TYPE_MOB, mob);
    }

    public void SpawnBullet(Vector3 position, Vector3 dir, bool ownerIsPlayer)
    {
        IPoolable poolableBullet = objectPoolController.GetObjectFromPool(ObjectPoolController.TYPE_BULLET);
        poolableBullet.Spawn(position, dir);
        poolableBullet.OnViewRemoved += OnBulletRemoved;
        objectPoolController.AddActiveObject(ObjectPoolController.TYPE_BULLET, poolableBullet);

        IBullet bullet = (IBullet)poolableBullet;
        bullet.OwnerIsPlayer = ownerIsPlayer;
        activeBullets.Add(bullet);
    }

    void OnBulletRemoved(IPoolable bullet)
    {
        bullet.OnViewRemoved -= OnBulletRemoved;
        objectPoolController.RemoveActiveObject(ObjectPoolController.TYPE_BULLET, bullet.View);
        objectPoolController.PushObjectInInactiveObjectPool(ObjectPoolController.TYPE_BULLET, bullet);
    }


    
    public RaycastHit[] MakeRaycastHit(Vector3 position, Vector3 dir, out int hitCount, float range, int layerMask)
    {
        hitCount = Physics.RaycastNonAlloc(position, dir, bulletRaycastHits, range, layerMask);
        // Debug.DrawLine(position, position + dir * range, Color.white, 2.0f);
        return bulletRaycastHits;
    }

    public IPoolable GetActiveObjectByType(string type, GameObject target)
    {
        return objectPoolController.GetActiveObject(ObjectPoolController.TYPE_MOB, target);
    }

    public IItem SpawnItem(ItemType type, Transform parent, bool ownerIsPlayer)
    {
        ItemView itemView = itemView = Instantiate(prefabItems[(int) type], parent.position, Quaternion.identity, parent);
        IItem item = null;
            
        switch (type)
        {
            case ItemType.Pistol:
            case ItemType.Riffle:
            {
                item = new Weapon(itemView, ownerIsPlayer);
            }
                break;
            default:
            {
                Debug.LogError("Can't spawn item: " + type);
                break;
            }
        }
        
        itemView.transform.localRotation = Quaternion.identity;
        itemView.transform.localPosition = Vector3.zero;
        return item;
    }

    public void DebugLog(object log, int level = 0)
    {
        if (level == 0) {
            Debug.Log(log);
        } else if (level == 1) {
            Debug.LogWarning(log);
        } else if (level == 2) {
            Debug.LogError(log);
        }
    }
}