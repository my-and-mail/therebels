using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponView : ItemView
{
    public Transform muzzle = null;
    public float rateOfFire = 0.5f;
}

public enum ItemType
{
    Pistol = 0,
    Riffle
}