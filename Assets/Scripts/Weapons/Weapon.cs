using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : IItem
{
    WeaponView view = null;

    private float fireTime = 0.0f;
    private bool ownerIsPlayer = false;

    public Weapon(ItemView newView, bool player)
    {
        view = (WeaponView)newView;
        ownerIsPlayer = player;
    }

    ~Weapon()
    {

    }

    public void Use(Vector3 enemyPos)
    {
        Shoot(enemyPos);
    }

    public void SetVisible(bool state)
    {
        view.gameObject.SetActive(state);
    }

    void Shoot(Vector3 enemyPos)
    {
        if (fireTime >= Time.time) {
            return;
        }

        Vector3 position = view.muzzle.position;
        Vector3 dir = (enemyPos - position).normalized;    
        LevelController.instance.SpawnBullet(position, dir, ownerIsPlayer);
        fireTime = Time.time + view.rateOfFire;
    }
}