using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : IBullet
{
    public event ObjectViewRemovedEvent OnViewRemoved;

    protected GameObject view = null;
    protected BulletData data = null;

    protected Vector3 startPosition = Vector3.zero;
    protected Vector3 oldPosition = Vector3.zero;
    protected bool ownerIsPlayer = false;

    public Bullet(BulletData newData, GameObject newView)
    {
        data = newData;
        view = newView;
    }

    ~Bullet()
    {
        view = null;
    }

    public void Spawn(Vector3 spawnPosition, Vector3 spawnDir)
    {
        startPosition = spawnPosition;
        view.transform.position = spawnPosition;
        view.transform.forward = spawnDir;
        view.SetActive(true);
        oldPosition = startPosition;
    }

    public void Remove()
    {
        view.SetActive(false);
        OnViewRemoved?.Invoke(this);
    }
    
    public int Damage
    {
        get => data.damage;
    }
    
    public float Range
    {
        get => data.range;
    }
    
    public float Speed
    {
        get => data.speed;
    }
    
    public GameObject View
    {
        get => view;
    }

    public bool OwnerIsPlayer
    {
        get => ownerIsPlayer;
        set => ownerIsPlayer = value;
    }
    
    public void Think()
    {
        oldPosition = view.transform.position;
        Vector3 newPosition = oldPosition + view.transform.forward * Speed * Time.deltaTime;

        if (Vector3.Distance(newPosition, startPosition) > Range) {
            Remove();
        } else {
            view.transform.position = newPosition;
            Vector3 dir = (newPosition - oldPosition);
            float range = dir.magnitude;
            dir.Normalize();
            int layerMask = ownerIsPlayer ? (1 << Layers.mob) : (1 << Layers.player);
            int hitCount = 0;
            
            RaycastHit[] raycastHits  = LevelController.instance.MakeRaycastHit(oldPosition, dir, out hitCount, range, layerMask);
            for (int i = 0; i < hitCount; i++) {
                if (ownerIsPlayer) {
                    IPoolable mob = LevelController.instance.GetActiveObjectByType(ObjectPoolController.TYPE_MOB,
                        raycastHits[i].collider.gameObject);
                    if (mob != null && mob is IDamageable)
                    {
                        IDamageable damageable = (IDamageable) mob;
                        damageable.TakeDamage(Damage);
                    }
                } else {
                    Player player = raycastHits[i].collider.GetComponent<Player>();
                    player.TakeDamage(Damage);
                }
                Remove();
            }
        }
    }

}