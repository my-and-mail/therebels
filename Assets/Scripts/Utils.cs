using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Layers
{
    public static int mob = 6;
    public static int player = 7;
    public static int findEnemy = 7;
}

public static class AnimHashes
{
    public static int speed = Animator.StringToHash("Speed");
    public static int shoot = Animator.StringToHash("Shoot");
    public static int death1 = Animator.StringToHash("Death1");
    public static int death2 = Animator.StringToHash("Death2");
    public static int death3 = Animator.StringToHash("Death3");
    public static int weapon = Animator.StringToHash("Weapon");
}

public static class MathEx
{
    public static float CalculateAngle(Vector3 from, Vector3 to) 
    {
         return Quaternion.FromToRotation(Vector3.up, to - from).eulerAngles.z;
    }

    /// <summary>Calculates angle between 2 vectors. Angle increases counter-clockwise.</summary>
    /// <param name="p1">1st point.</param>
    /// <param name="p2">2nd point.</param>
    /// <param name="o">Starting position of vectors.</param>
    /// <returns>Angle between 0° and 360°.</returns>
    public static float Angle360(Vector2 p1, Vector2 p2, Vector2 o = default(Vector2))
    {
        Vector2 v1, v2;
        if (o == default(Vector2))
        {
            v1 = p1.normalized;
            v2 = p2.normalized;
        }
        else
        {
            v1 = (p1 - o).normalized;
            v2 = (p2 - o).normalized;
        }
        float angle = Vector2.Angle(v1, v2);
        return Mathf.Sign(Vector3.Cross(v1, v2).z) < 0 ? (360 - angle) % 360 : angle;
    }
}