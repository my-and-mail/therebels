using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIEnemyIndicator : MonoBehaviour
{
    public RectTransform rect = null;
    public RectTransform arrow = null;
    public TextMeshProUGUI textEnemyCount = null;

    public float GetSize()
    {
        return rect.sizeDelta.x + arrow.sizeDelta.x;
    }
}
