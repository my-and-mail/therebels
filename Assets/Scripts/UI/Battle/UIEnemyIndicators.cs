using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEnemyIndicators : MonoBehaviour
{
    public UIEnemyIndicator uiPrefabEnemyIndicator = null;

    List<UIEnemyIndicator> uiIndicators = new ();
    float updateTime = 0.0f;

    Camera mainCamera = null;
    Vector2 enemyDir = Vector2.zero;
    float indicatorSize = 0.0f;
    float indicatorArrowSize = 0.0f;
    Dictionary<int, Vector2> anglesToEnemy = new ();
    Dictionary<int, int> anglesToEnemyCount = new ();
    Dictionary<int, int> anglesToEnemyAngles = new ();

    public void Init(int count)
    {
        mainCamera = Camera.main;

        UIEnemyIndicator uiEnemyIndicator = null;
        for(int i = 0; i < count; i++) {
            uiEnemyIndicator = GameObject.Instantiate(uiPrefabEnemyIndicator, transform.position, Quaternion.identity, transform);
            uiEnemyIndicator.rect.anchoredPosition = Vector2.zero;
            uiEnemyIndicator.gameObject.SetActive(false);
            uiIndicators.Add(uiEnemyIndicator);
        }
        indicatorSize = uiEnemyIndicator.GetSize();
        indicatorArrowSize = uiEnemyIndicator.arrow.sizeDelta.x;
    }

    void Update()
    {
        if (updateTime >= Time.time) {
            return;
        }

        updateTime = Time.time + 0.1f;

        anglesToEnemy.Clear();
        anglesToEnemyCount.Clear();
        anglesToEnemyAngles.Clear();
        List<IMob> mobs = LevelController.instance.ActiveMobs;
        for (int i = 0; i < mobs.Count; i++) {
            IMob mob = mobs[i];
            if (!mob.IsDead()) {
                Vector3 screenPos = mainCamera.WorldToScreenPoint(mob.View.transform.position);
                bool visible = false;
                if (screenPos.x < 0.0f) {
                    screenPos.x = indicatorArrowSize;
                    visible = true;
                }
                else if (screenPos.x > Screen.width) {
                    screenPos.x = Screen.width - indicatorSize;
                    visible = true;
                }

                if (screenPos.y < 0.0f) {
                    screenPos.y = indicatorArrowSize;
                    visible = true;
                }
                else if (screenPos.y > Screen.height) {
                    screenPos.y = Screen.height - indicatorSize;
                    visible = true;
                }

                if (visible) {
                    enemyDir.x = screenPos.x - Screen.width / 2.0f;
                    enemyDir.y = screenPos.y - Screen.height / 2.0f;
                    enemyDir.Normalize();
                    int angle = (int)MathEx.Angle360(Vector2.right, enemyDir);
                    int roundAngle = angle / 15; // округляем по 15 градусов
                    if (!anglesToEnemy.ContainsKey(roundAngle)) {
                        anglesToEnemy.Add(roundAngle, screenPos);
                        anglesToEnemyCount.Add(roundAngle, 1);
                        anglesToEnemyAngles.Add(roundAngle, angle);
                    } else {
                        anglesToEnemyCount[roundAngle]++;
                    }
                }
            }
        }

        int indicatorIndex = 0;
        foreach (var angle in anglesToEnemy.Keys) {
            SetUIIndicator(anglesToEnemyAngles[angle], anglesToEnemy[angle], indicatorIndex, anglesToEnemyCount[angle]);
            indicatorIndex++;
        }

        for (int i = indicatorIndex; i < uiIndicators.Count; i++) {
            uiIndicators[i].gameObject.SetActive(false);
        }
    }

    void SetUIIndicator(float angle, Vector2 screenPos, int index, int count)
    {
        UIEnemyIndicator indicator = GetUIIndicator(index);
        if (indicator != null) {
            indicator.gameObject.SetActive(true);
            indicator.rect.anchoredPosition = screenPos;
            indicator.textEnemyCount.text = $"{count}";
            Vector3 currentEulerAngles = indicator.arrow.transform.eulerAngles;
            currentEulerAngles.z = angle;
            indicator.arrow.eulerAngles = currentEulerAngles;
        }        
    }

    UIEnemyIndicator GetUIIndicator(int index)
    {
        if (index >= uiIndicators.Count) {
            Debug.LogError("GetActiveIndicator() is failed!");
            return null;
        }

        return uiIndicators[index];
    }
}
