using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolController
{
    public static readonly string TYPE_MOB = "mob";
    public static readonly string TYPE_BULLET = "bullet";
    public static readonly string TYPE_ENEMY_INDICATOR = "enemyIndicator";

    Dictionary <string, ObjectPool<IPoolable>> inactiveObjects = new Dictionary<string, ObjectPool<IPoolable>>();

    Dictionary <string, Dictionary <GameObject, IPoolable>> activeObjects = new Dictionary <string, Dictionary <GameObject, IPoolable>>();


    public IPoolable GetObjectFromPool(string type)
    {
        ObjectPool<IPoolable> objectPool = GetInactiveObjectPool(type);
        if (objectPool != null) {
            IPoolable newObj = null;
            if (objectPool.TryPop(out newObj)) {
                return newObj;
            } else {
                // debug
            }
        }

        //Debug.LogError(); // TODO
        return null;
    }

    public int GetInactiveObjectCount(string type)
    {
        ObjectPool<IPoolable> objectPool = GetInactiveObjectPool(type);
        if (objectPool != null) {
            return objectPool.Count;
        }  

        return 0;    
    }

    public void PushObjectInInactiveObjectPool(string type, IPoolable obj)
    {
        ObjectPool<IPoolable> objectPool = GetInactiveObjectPool(type);
        if (objectPool != null) {
            objectPool.Push(obj);
        }
    }

    ObjectPool<IPoolable> GetInactiveObjectPool(string type)
    {
        if (inactiveObjects.ContainsKey(type)) {
            return inactiveObjects[type];
        }

        ObjectPool<IPoolable> objectPool = new ObjectPool<IPoolable>();
        inactiveObjects.Add(type, objectPool);

        return objectPool;    
    }

    // Active objects pool
    public void AddActiveObject(string type, IPoolable obj)
    {
        Dictionary <GameObject, IPoolable> data = null;

        if (activeObjects.ContainsKey(type)) {
            data = activeObjects[type];
        } else {
            data = new Dictionary<GameObject, IPoolable>();
            activeObjects.Add(type, data);
        }

        data.Add(obj.View, obj);
    }

    public IPoolable GetActiveObject(string type, GameObject view)
    {
        if (!activeObjects.ContainsKey(type)) {
            Debug.LogError("GetActiveObject: type is not found! type: " + type);
            return null;
        }

        Dictionary <GameObject, IPoolable> data = activeObjects[type];
        if (!data.ContainsKey(view)) {
            Debug.LogError("GetActiveObject: gameobject is not found! type: " + type + " " + view);
            return null;
        }

        return data[view];
    }

    public void RemoveActiveObject(string type, GameObject view)
    {
        if (!activeObjects.ContainsKey(type)) {
            return;
        }

        Dictionary <GameObject, IPoolable> data = activeObjects[type];
        if (!data.ContainsKey(view)) {
            return;
        }

        data.Remove(view);
    }
}

public interface IPoolable
{
    void Spawn(Vector3 startPosition, Vector3 dir);
    GameObject View
    {
        get;
    }
    event ObjectViewRemovedEvent OnViewRemoved;
}