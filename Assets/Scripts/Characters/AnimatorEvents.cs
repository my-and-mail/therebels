using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEvents : MonoBehaviour
{
    public Animator animator = null;
    
    [System.NonSerialized]
    public bool shootAnimationReady = false;

    public void ShootStarted_AnimEvent()
    {
        shootAnimationReady = true;
    }

    public void Death_AnimEvent()
    {
        animator.enabled = false;
    }
}
