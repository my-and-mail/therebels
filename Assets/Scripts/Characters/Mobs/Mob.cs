using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob : IMob
{
    public event ObjectViewRemovedEvent OnViewRemoved;

    protected MobView view = null;
    protected Vector3 startPosition = Vector3.zero;

    int health = 10;
    float removeDeadBodyTime = 0.0f;
    Weapon weapon = null;
    bool canShootWeapon = false;
    IDamageable nearestEnemy = null;
    float findEnemyTime = 0.0f;

    public Mob(MobView newView)
    {
        view = newView;
        view.Mob = this;

        int weaponType = (int) view.currentWeapon;
        weapon = (Weapon)LevelController.instance.SpawnItem(view.currentWeapon, view.weaponAnchors[weaponType], false);
    }

    ~Mob()
    {
        view.Mob = null;
        view = null;
    }

    public void Spawn(Vector3 spawnPosition, Vector3 spawnDir)
    {
        startPosition = spawnPosition;
        view.transform.position = spawnPosition;
        view.transform.forward = spawnDir;
        view.gameObject.SetActive(true);
        int weaponType = (int) view.currentWeapon;
        view.SetAnimatorInt(AnimHashes.weapon, weaponType);
    }

    void Remove()
    {
        view.gameObject.SetActive(false);
        OnViewRemoved?.Invoke(this);
    }

    public float GetSpeed()
    {
        return 10.0f;
    }

    public GameObject View
    {
        get => view.gameObject;
    }

    public void Think()
    {
        if (IsDead()) {
            if (removeDeadBodyTime < Time.time) {
                removeDeadBodyTime = 0.0f;
                Remove();
            }
            return;
        }
        
        if (weapon != null && canShootWeapon && view.animatorEvents.shootAnimationReady) {
            weapon.Use(nearestEnemy.Transform.position);
        }
        
        FindEnemy();
        AttackEnemy();

        // view.transform.position += view.transform.forward * GetSpeed() * Time.deltaTime;

        // if (Vector3.Distance(view.transform.position, startPosition) > GetRange()) {
        //     Remove();
        // }
    }

    void FindEnemy()
    {
        if (findEnemyTime >= Time.time) {
            return;
        }
        findEnemyTime = Time.time + 0.1f;
        // Vector3 direction = inputController.GetMoveDir();
        // if (direction != Vector3.zero) {
        //     ClearEnemy();
        //     return;
        // }
        
        if (nearestEnemy == null) {
            nearestEnemy = FindEnemyInRange();
        }

        if (nearestEnemy != null) {
            if (nearestEnemy.IsDead()) {
                ClearEnemy();
            } else {
                float d = Vector3.Distance(nearestEnemy.Transform.position, view.transform.position);
                if (d >= 5.0f)
                {
                    ClearEnemy();
                }
            }
        }
    }

    IDamageable FindEnemyInRange()
    {
        Player player = LevelController.instance.MainPlayer;
        if (player.IsDead()) {
            return null;
        }

        float d = Vector3.Distance(player.transform.position, view.transform.position);

        if (d < 5.0f) {
            return player;
        }
        
        return null;
    }

    void ClearEnemy()
    {
        nearestEnemy = null;
        canShootWeapon = false;
        view.SetAnimatorBool(AnimHashes.shoot, false);
        view.animatorEvents.shootAnimationReady = false;
    }

    void AttackEnemy()
    {
        if (nearestEnemy == null) {
            return;
        }

        canShootWeapon = view.AimToEnemy(nearestEnemy.Transform.position);
    }
    
    public void Death()
    {
        removeDeadBodyTime = Time.time + 5.0f;
        view.SetDeath();
    }
    

    // ****************************
    // IDamageable
    // ****************************
    public void TakeDamage(int damage)
    {
        if (IsDead()) {
            return;
        }

        health -= damage;

        if (health <= 0) {
            Death();
        }
    }
    
    public bool IsDead()
    {
        return health <= 0;
    }
    
    public Transform Transform
    {
        get => view.transform;
    }
}
