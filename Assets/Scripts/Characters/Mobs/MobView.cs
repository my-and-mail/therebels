using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobView : MonoBehaviour
{
    public Animator animator = null;
    public CapsuleCollider collider = null;
    public ItemType currentWeapon = ItemType.Pistol;
    public Transform[] weaponAnchors;
    public AnimatorEvents animatorEvents;
    
    private Mob mob;
    
    public Mob Mob
    {
        get { return mob; }
        set { mob = value; }
    }

    public void SetDeath()
    {
        animator.applyRootMotion = true;
        collider.enabled = false;
        int rnd = Random.Range(0, 3);
        if (rnd == 1) {
            animator.SetTrigger(AnimHashes.death1);
        } else if (rnd == 2) {
            animator.SetTrigger(AnimHashes.death2);
        } else {
            animator.SetTrigger(AnimHashes.death3);
        }
    }

    public bool AimToEnemy(Vector3 targetPos)
    {
        Transform viewTransform = transform;
        Vector3 position = viewTransform.position;
        Vector3 enemyPos = targetPos;
        Vector3 dirToEnemy = (enemyPos - position).normalized;
        dirToEnemy.y = 0.0f;

        Quaternion targetRotation = Quaternion.LookRotation(dirToEnemy);
        viewTransform.rotation = Quaternion.RotateTowards(viewTransform.rotation, targetRotation, Time.deltaTime * 250.0f);

        Vector3 lookDir = viewTransform.forward;
        lookDir.y = 0.0f;
        SetAnimatorBool(AnimHashes.shoot, true);
        return Vector3.Angle(dirToEnemy, lookDir) < 5;
    }

    public void SetAnimatorBool(int hash, bool state)
    {
        animator.SetBool(hash, state);
    }
    
    public void SetAnimatorInt(int hash, int state)
    {
        animator.SetInteger(hash, state);
    }
}
