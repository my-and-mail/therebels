using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindEnemySensor : MonoBehaviour
{
    List<IDamageable> targets = new List<IDamageable>();
    
    public List<IDamageable> Targets
    {
        get { return targets; }
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject go = other.gameObject;
        if (go.layer == Layers.mob) {
            MobView target = go.GetComponent<MobView>();
            if (!target.Mob.IsDead()) {
                // Debug.Log(other.name + " IN");
                targets.Add(target.Mob);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject go = other.gameObject;
        if (go.layer == Layers.mob) {
            // Debug.Log(other.name + " OUT");
            ClearTarget(go.GetComponent<MobView>().Mob);
        }
    }

    public void ClearTarget(IDamageable target)
    {
        targets.Remove(target);
    }
}
