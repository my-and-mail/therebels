﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchInputController : IInputController
{
	float moveButtonSize = 100.0f;
	float actionButtonSize = 64.0f;

	protected Dictionary <string, int> inputTouchFinger = new Dictionary <string, int>();

    Vector3 moveDir = Vector3.zero;

    RectTransform moveIDRect;
	RectTransform moveSizeTouchRect;
	RectTransform moveTouchRect;
	Camera uiCamera;
	bool simulate = false;
	Vector2 moveRectHalfSize = Vector2.zero;

	public TouchInputController(RectTransform moveRect, bool simulateTouchInput, Camera camera)
	{
		simulate = simulateTouchInput;
		uiCamera = camera;
		moveIDRect = moveRect;
        moveSizeTouchRect = moveRect.GetChild(0).GetComponent<RectTransform>();
        moveTouchRect = moveRect.GetChild(1).GetComponent<RectTransform>();
        inputTouchFinger.Add("Move", -1);
        moveRectHalfSize = moveIDRect.sizeDelta / 2.0f;
	}
	
	public void Think()
	{
		if (LevelController.instance == null) {
			return;
		}

		Player player = LevelController.instance.MainPlayer;

        if (player.IsDead()) {
            return;
        }

        if (simulate)
        {
	        Vector2 touchPos = Input.mousePosition;
	        if (Input.GetMouseButtonDown(0))
	        {
		        if (IsTouchInMobileRect(touchPos))
		        {
			        Vector2 localPos;
			     //    RectTransformUtility.ScreenPointToLocalPointInRectangle(LevelController.instance.mobileInputRect, 
				    // touchPos, uiCamera, out localPos);
			        //Debug.Log(touchPos + " " + localPos);
			        moveIDRect.anchoredPosition = touchPos - moveRectHalfSize;
			        inputTouchFinger["Move"] = 1;
		        }
	        } else if (Input.GetMouseButton(0))
	        {
		        if (IsTouchInMobileRect(touchPos))
		        {
			        CalculateMoveTouchAngle(touchPos);
		        }
	        }
	        else
	        {
		        moveDir = Vector3.zero;
		        ResetJoystickPos();
	        }
        }
        else
        {
	        if (Input.touchCount > 0)
	        {
		        for (var i = 0; i < Input.touchCount; ++i)
		        {
			        Touch touch = Input.GetTouch(i);

			        int fingerId = touch.fingerId;
			        Vector2 touchPos = touch.position;

			        switch (touch.phase)
			        {
				        // Record initial touch position.
				        case TouchPhase.Began:
					        if (IsTouchInMobileRect(touchPos))
					        {
						        moveIDRect.anchoredPosition = touchPos - moveRectHalfSize;
						        inputTouchFinger["Move"] = fingerId;
					        }
					        break;

				        // Determine direction by comparing the current touch position with the initial one.
				        case TouchPhase.Moved:
					        if (inputTouchFinger["Move"] == fingerId)
					        {
						        CalculateMoveTouchAngle(touchPos);
					        }

					        break;
				        case TouchPhase.Stationary:
					        // if (inputTouchFinger["Move"] == fingerId)
					        // {
						       //  moveDir = Vector3.zero;
					        // }

					        break;
				        // Report that a direction has been chosen when the finger is lifted.
				        case TouchPhase.Ended:
				        case TouchPhase.Canceled: // есть еще в TouchEnd()
					        if (inputTouchFinger["Move"] == fingerId)
					        {
						        moveDir = Vector3.zero;
						        inputTouchFinger["Move"] = -1;
						        ResetJoystickPos();
					        }

					        break;
			        }
		        }
	        }
        }
	}

	void CalculateMoveTouchAngle(Vector2 pos)
	{
        Vector2 center = moveIDRect.rect.size / 2.0f;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(moveIDRect, pos, uiCamera, out pos);
        
        Vector2 joistickPos = pos - center;
        Vector2 dir = joistickPos.normalized;
        float d = joistickPos.magnitude;

        if (d > 25.0f) {
            joistickPos = dir * 25.0f;
        }
        moveTouchRect.anchoredPosition = joistickPos;
        moveDir.x = dir.x;
        moveDir.z = dir.y;
	}

    void ResetJoystickPos()
    {
	    moveTouchRect.anchoredPosition = Vector2.zero;
    }
	
	protected bool IsTouchPressed()
	{ 
#if UNITY_EDITOR
        return Input.GetMouseButton(0) || Input.GetMouseButton(1);
#else
		return Input.touchCount > 0;
#endif
	}
	
	public Vector3 GetMoveDir()
	{
		if (!IsTouchPressed()) {
			return Vector3.zero;
		}

		return moveDir;
	}

	public bool IsTouchInMobileRect(Vector2 position)
	{
		//return Vector2.Distance (position, GetMovingTouchControlCenterPos ()) < moveButtonSize * 2;
        return RectTransformUtility.RectangleContainsScreenPoint(LevelController.instance.mobileInputRect, position, uiCamera);
	}
	
	public bool IsShootButtonPressed()
	{
		return false;
	}
}
