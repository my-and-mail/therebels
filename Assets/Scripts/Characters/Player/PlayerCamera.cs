using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Transform lookTarget = null;
    public float smoothSpeed = 3.0f;

    float targetOffset = 0.0f;

    void Start()
    {
        float downAngle = Vector3.Angle(transform.forward, Vector3.down);
        targetOffset = (transform.position.y - lookTarget.position.y) * Mathf.Tan(downAngle * Mathf.Deg2Rad); // По прямоугольному треугольнику находим насколько надо отодвигать камеру от точки, куда смотрим
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.DrawLine(transform.position, transform.position + transform.forward * 30.0f, Color.red);
        float yPos = transform.position.y;
        Vector3 targetPosition = lookTarget.position + Vector3.back * targetOffset;
        targetPosition.y = yPos;
        transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * smoothSpeed);
    }
}
