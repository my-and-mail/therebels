using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInputController : IInputController
{
    Vector3 moveDir = Vector3.zero;

    public KeyboardInputController()
    {

    }

    ~KeyboardInputController()
    {

    }

    public Vector3 GetMoveDir()
    {
        return moveDir;
    }

    public bool IsShootButtonPressed()
    {
        if (Input.GetKeyDown(KeyCode.Z)) {
            return true;
        }

        return false;
    }

    public void Think()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        moveDir.x = h;
        moveDir.z = v;
    }
}