using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamageable
{
    public Animator animator = null;
    public CapsuleCollider collider = null;
    public bool simulateTouchInput = false; 
    public float moveSpeed = 10.0f;
    public float rotationSpeed = 180.0f;
    public Transform[] weaponAnchors;
    public FindEnemySensor findEnemySensor = null;
    public AnimatorEvents animatorEvents = null;
    public int health = 50;
    public ItemType[] initialInventory;

    IInputController inputController = null;
    Weapon currentWeapon;
    IDamageable nearestEnemy = null;
    bool canShootWeapon = false;
    Dictionary<ItemType, IItem> inventory = new Dictionary<ItemType, IItem>();

    void Awake()
    {
#if UNITY_EDITOR
        if (simulateTouchInput) {
            inputController = new TouchInputController(LevelController.instance.moveImageBackground, simulateTouchInput, 
                LevelController.instance.uiCamera);
        } else {
            inputController = new KeyboardInputController();
        }

#else
        inputController = new TouchInputController(LevelController.instance.moveImageBackground, false, LevelController.instance.uiCamera);
#endif

        for (int i = 0; i < initialInventory.Length; i++) {
            ItemType type = initialInventory[i];
            IItem item = LevelController.instance.SpawnItem(type, weaponAnchors[(int)type],true);
            item.SetVisible(false);
            inventory.Add(type, item);
        }

        SwitchToWeapon(initialInventory[0]);
    }

    void SwitchToWeapon(ItemType type)
    {
        if (currentWeapon != null) {
            currentWeapon.SetVisible(false);
        }
        
        currentWeapon = (Weapon)inventory[type];
        currentWeapon.SetVisible(true);
        animator.SetInteger(AnimHashes.weapon, (int)type);
    }

    void Update()
    {
        if (IsDead()) {
            return;
        }

        inputController.Think();

        if (currentWeapon != null && canShootWeapon && animatorEvents.shootAnimationReady) {
            currentWeapon.Use(nearestEnemy.Transform.position);
        }

        Move();
        FindEnemy();
        AttackEnemy();
    }

    void Move()
    {
        Vector3 direction = inputController.GetMoveDir();
        if (direction != Vector3.zero) {
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
            transform.position += moveSpeed * Time.deltaTime * transform.forward;
            animator.SetFloat(AnimHashes.speed, moveSpeed);
        } else {
            animator.SetFloat(AnimHashes.speed, 0.0f);
        }
    }

    void FindEnemy()
    {
        Vector3 direction = inputController.GetMoveDir();
        if (direction != Vector3.zero) {
            ClearEnemy();
            return;
        }
        
        if (nearestEnemy == null) {
            nearestEnemy = FindEnemyInRange();
        }

        if (nearestEnemy != null) {
            if (nearestEnemy.IsDead()) {
                findEnemySensor.ClearTarget(nearestEnemy);
                ClearEnemy();
            }
        }
    }

    void ClearEnemy()
    {
        nearestEnemy = null;
        canShootWeapon = false;
        animator.SetBool(AnimHashes.shoot, false);
        animatorEvents.shootAnimationReady = false;
    }

    IDamageable FindEnemyInRange()
    {
        Vector3 position = transform.position;
        float minDistance = float.MaxValue;
        List<IDamageable> targets = findEnemySensor.Targets;

        for (int i = 0; i < targets.Count; i++) {
            float d = Vector3.Distance(targets[i].Transform.position, position);
            if (d < minDistance) {
                minDistance = d;
                nearestEnemy = targets[i];
            }
        }

        return nearestEnemy;
    }

    void AttackEnemy()
    {
        if (nearestEnemy == null) {
            return;
        }

        Vector3 position = transform.position;
        Vector3 enemyPos = nearestEnemy.Transform.position;
        Vector3 dirToEnemy = (enemyPos - position).normalized;
        dirToEnemy.y = 0.0f;

        Quaternion targetRotation = Quaternion.LookRotation(dirToEnemy);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);

        Vector3 lookDir = transform.forward;
        lookDir.y = 0.0f;
        float angle = Vector3.Angle(dirToEnemy, lookDir);
        canShootWeapon = angle < 5;
        animator.SetBool(AnimHashes.shoot, true);
    }

    public void Death()
    {
        animator.applyRootMotion = true;
        collider.enabled = false;
        int rnd = Random.Range(0, 3);
        if (rnd == 1) {
            animator.SetTrigger(AnimHashes.death1);
        } else if (rnd == 2) {
            animator.SetTrigger(AnimHashes.death2);
        } else {
            animator.SetTrigger(AnimHashes.death3);
        }
    }
    
    // ****************************
    // IDamageable
    // ****************************
    public void TakeDamage(int damage)
    {
        if (IsDead()) {
            return;
        }

        health -= damage;

        if (IsDead()) {
            Death();
        }
    }

    public bool IsDead()
    {
        return health <= 0;
    }

    public Transform Transform
    {
        get => transform;
    }
}


