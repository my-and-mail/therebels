﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "BulletData", menuName = "Bullet Data", order = 51)]
public class BulletData : ScriptableObject
{
	public int damage = 20;
	public float range = 15.0f;
	public float speed = 20.0f;

}
