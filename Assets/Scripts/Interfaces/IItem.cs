﻿using UnityEngine;

public interface IItem
{
    void Use(Vector3 enemyPos);
    void SetVisible(bool state);
}
