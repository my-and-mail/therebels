using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputController
{
    Vector3 GetMoveDir();
    void Think();
    bool IsShootButtonPressed();
}