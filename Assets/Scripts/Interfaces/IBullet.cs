using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBullet : IPoolable
{
    int Damage { get; }
    float Range { get; }
    float Speed { get; }
    bool OwnerIsPlayer { get; set; }

    void Think();
}