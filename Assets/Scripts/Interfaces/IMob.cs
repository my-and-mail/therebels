using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMob : IDamageable, IPoolable
{
    void Think();
    void Death();
}

public delegate void ObjectViewRemovedEvent(IPoolable obj);