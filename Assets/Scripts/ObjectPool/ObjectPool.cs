using System.Collections;
using System.Collections.Generic;

public class ObjectPool<T>
{
    Stack<T> pool = new Stack<T>();

    public int Count
    {
        get {return pool.Count;}
    }

    public void Push(T obj)
    {
        pool.Push(obj);
    }

    public bool TryPop(out T obj)
    {
        if (pool.Count > 0) {
            obj = pool.Pop();
            return true;
        }

        obj = default(T);
        return false;
    }

}
