using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Utility
{
    [RequireComponent(typeof (TextMeshProUGUI))]
    public class FPSCounter : MonoBehaviour
    {
        const float fpsMeasurePeriod = 0.5f;
        private int m_FpsAccumulator = 0;
        private float m_FpsNextPeriod = 0;
        private int m_CurrentFps;
        public int FPS
        {
            get {return m_CurrentFps;}
        }
        const string display = "{0} FPS";
        private TextMeshProUGUI m_Text;

        List<int> fpsData = new List<int>();
        float fpsDataTime = 0.0f;

        private void Start()
        {
            m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
            m_Text = GetComponent<TextMeshProUGUI>();
            fpsDataTime = Time.time + 5.0f; // дать прогрузиться
        }


        private void Update()
        {
            if (Time.timeScale == 0.0f) {
                return;
            }

            // measure average frames per second
            m_FpsAccumulator++;
            if (Time.realtimeSinceStartup > m_FpsNextPeriod)
            {
                m_CurrentFps = (int) (m_FpsAccumulator/fpsMeasurePeriod);
                m_FpsAccumulator = 0;
                m_FpsNextPeriod += fpsMeasurePeriod;
                m_Text.text = string.Format(display, m_CurrentFps);

                if (fpsDataTime < Time.time) {
                    fpsDataTime = Time.time + 10.0f;
                    fpsData.Add(m_CurrentFps);

                    if (fpsData.Count >= 6) {
                        fpsData.Remove(0);
                    }
                }
            }
        }

        public int GetAverageFPS()
        {
            if (fpsData.Count == 0) {
                return 60;
            }

            int fpsValue = 0;
            for(int i = 0; i < fpsData.Count; i++) {
                fpsValue += fpsData[i];
            }

            return (fpsValue / fpsData.Count);
        }
    }
}
